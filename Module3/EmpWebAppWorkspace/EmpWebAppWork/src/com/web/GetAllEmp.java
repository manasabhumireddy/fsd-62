package com.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.EmployeeDao;
import com.dto.Employee;

@WebServlet("/GetAllEmp")
public class GetAllEmp extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        
        // Retrieving all employees
        EmployeeDao empDao = new EmployeeDao();
        List<Employee> empList = empDao.getAllEmployees();
        
        out.println("<html>");
        out.println("<body bgcolor='lightyellow' text='green'>");
        
        RequestDispatcher rd = request.getRequestDispatcher("HRHomePage");
        rd.include(request, response);
        
        out.println("<br/>");
        out.println("<center>");
        if (!empList.isEmpty()) {
            out.println("<table align='center' border='2'>");
            out.println("<tr>");
            out.println("<th>EmpId</th>");
            out.println("<th>EmpName</th>");
            out.println("<th>Salary</th>");
            
            out.println("<th>EmailId</th>");
            out.println("<th>Gender</th>");
            out.println("</tr>");
            
            for (Employee emp : empList) {
                out.println("<tr>");
                out.println("<td>" + emp.getEmpId()   + "</td>");
                out.println("<td>" + emp.getEmpName() + "</td>");
                out.println("<td>" + emp.getSalary()  + "</td>");
                out.println("<td>" + emp.getEmailId() + "</td>");
                out.println("<td>" + emp.getGender()  + "</td>");
                out.println("</tr>");
            }
            
            out.println("</table>");
        } else {
            out.println("<h3 style='color:red;'>No Employee Records Found!!!</h3>");
        }
        out.println("</center></body></html>");
	}
	
	

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
	}

}
