package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.EmployeeDao;
import com.dto.Employee;

@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		String emailId = request.getParameter("emailId");
		String password = request.getParameter("password");

		out.println("<html>");
		out.println("<body bgcolor='lightyellow'><center>");

		if (emailId.equalsIgnoreCase("HR") && password.equals("HR")) {
			RequestDispatcher rd = request.getRequestDispatcher("HRHomePage");
			rd.forward(request, response);			
		} else {

			//For Employee login
			EmployeeDao empDao = new EmployeeDao();
			Employee emp = empDao.empLogin(emailId, password);
			
			if (emp != null) {
				RequestDispatcher rd = request.getRequestDispatcher("EmpHomePage");
				rd.forward(request, response);	
			} else {
				out.println("<h1 style='color:red;'>Invalid Credentials</h1>");			
				RequestDispatcher rd = request.getRequestDispatcher("Login.html");
				rd.include(request, response);		
			}			
			//For Employee login
		}

		out.println("</center></body></html>");		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}



