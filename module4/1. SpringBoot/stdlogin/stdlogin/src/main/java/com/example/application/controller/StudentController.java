package com.example.application.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.example.application.entity.Student;
import com.example.application.service.StudentService;

@RestController
@RequestMapping("/students")
public class StudentController {

    @Autowired
    private StudentService studentService;

    // Register a new student
    @PostMapping("/register")
    public Student registerStudent(@RequestBody Student student) {
        return studentService.registerStudent(student);
    }

    // Login
    @PostMapping("/login")
    public String login(@RequestBody StudentLoginRequest loginRequest) {
        boolean isValid = studentService.login(loginRequest.getEmailId(), loginRequest.getPassword());
        return isValid ? "Login successful" : "Invalid email or password";
    }
}

class StudentLoginRequest {
    private String emailId;
    private String password;

    // Getters and setters

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
