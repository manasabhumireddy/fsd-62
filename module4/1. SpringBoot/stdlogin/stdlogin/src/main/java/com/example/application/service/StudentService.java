package com.example.application.service;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.application.entity.Student;
import com.example.application.repository.StudentRepository;

@Service
public class StudentService {

    @Autowired
    private StudentRepository studentRepository;

    // Register a new student
    public Student registerStudent(Student student) {
        // Hash the password before saving it to the database
        student.setPassword(BCrypt.hashpw(student.getPassword(), BCrypt.gensalt()));
        return studentRepository.save(student);
    }

    // Validate login
    public boolean login(String emailId, String password) {
        Student student = studentRepository.findByEmailId(emailId);
        if (student != null) {
            // Compare the provided password with the hashed password in the database
            return BCrypt.checkpw(password, student.getPassword());
        }
        return false;
    }
}

