
package com.example.application.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.example.application.entity.Student;

public interface StudentRepository extends JpaRepository<Student, Long> {
    Student findByEmailId(String emailId);
}
