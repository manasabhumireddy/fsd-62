package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Product;

@Service
public class ProductDao {

	@Autowired
	ProductRepository productRepo;
	

	public List<Product> getAllProducts() {
		return productRepo.findAll();
	}

	public Product getProductById(int productId) {
		return productRepo.findById(productId).orElse(null);
	}

	public List<Product> getProductByName(String productName) {
		return productRepo.findByName(productName);
	}

	public Product addProduct(Product product) {
		return productRepo.save(product);
	}
	
	public Product updateProduct(Product product) {
		return productRepo.save(product);
	}

	public void deleteProductById(int productId) {
		productRepo.deleteById(productId);
	}
	
}