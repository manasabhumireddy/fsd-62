package com.controllers;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


import com.dao.StudentDao;
import com.model.Student;

@RestController
public class StudentController {

	@Autowired
	StudentDao stdDao;
		@PostMapping("stdLogin")
	    public Student stdLogin(@RequestBody Student credentials) {
	        String emailId = credentials.getEmailId();
	        String password = credentials.getPassword();
	        return stdDao.stdLogin(emailId, password);
	    }
	
//		@GetMapping("stdLogin/{emailId}/{password}")
//		public Student empLogin(@PathVariable("emailId") String emailId, 
//				@PathVariable("password") String password) {
//			
//			return stdDao.stdLogin(emailId, password);
//		}

	
		
	
	
//	@PostMapping("stdLogin")
//	public ResponseEntity<?> stdLogin(@RequestBody Student credentials) {
//	   
//	    String emailId = credentials.getEmailId();
//	    String passwordToCheck = credentials.getPassword();
//	    Student loggedInStudent = stdDao.stdLogin(emailId, passwordToCheck);
//	    
//	    
//	    if (loggedInStudent != null) {
//	        return ResponseEntity.ok(loggedInStudent);
//	    } else {
//	        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Invalid credentials");
//	    }
//	}
	
//	@PostMapping("stdLogin/{emailId}/{password}")
//	public Student empLogin(@PathVariable("emailId") String emailId, 
//			@PathVariable("password") String password) {
//		String encryptedpassword = null; 
//		  try   
//	        {  
//	            MessageDigest m = MessageDigest.getInstance("MD5"); 
//	            m.update(password.getBytes()); 
//	            byte[] bytes = m.digest();  
//	            StringBuilder s = new StringBuilder();  
//	            for(int i=0; i< bytes.length ;i++)  
//	            {  
//	                s.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));  
//	            }    
//	            encryptedpassword = s.toString();  
//	        }   
//	        catch (NoSuchAlgorithmException e)   
//	        {  
//	            e.printStackTrace();  
//	        }  
//		
//		System.out.println(encryptedpassword);
//		
//		return stdDao.stdLogin(emailId,encryptedpassword );
//	}
	
	
	@GetMapping("getAllStudents")
	public List<Student> getAllStudents() {
		return stdDao.getAllStudents();
	}

	@GetMapping("getStudentById/{id}")
	public Student getStudentById(@PathVariable("id") int stdId) {
		return stdDao.getStudentById(stdId);
	}

//	@GetMapping("getStudentByName/{name}")
//	public List<Student> getStudentByName(@PathVariable("name") String stdName) {
//		return stdDao.getStudentByName(stdName);
//	}

	@PostMapping("addStudent")
	public ResponseEntity<?> addStudent(@RequestBody Student std) {
		try {
			return ResponseEntity.ok(stdDao.addStudent(std));
		} catch (DataIntegrityViolationException e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Email ID already exists");
		}
	}

	@PutMapping("updateStudent")
	public Student updateStudent(@RequestBody Student std) {
		return stdDao.updateStudent(std);
	}

	@DeleteMapping("deleteStudentById/{id}")
	public String deleteStudentById(@PathVariable("id") int stdId) {
		stdDao.deleteStudentById(stdId);
		return "Student Record Deleted Successfully!!!";
	}
}







