package com.model;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.mindrot.jbcrypt.BCrypt;

@Entity
public class Student {

	@Id@GeneratedValue
	private int stdId;
	private String stdName;
	private String gender;
	private String country;
	private Date dob;
	
	@Column(unique = true)
	private String emailId;
	
	private String password;
	
	@ManyToOne
	@JoinColumn(name="courseId")
	Course Course;
	
	public Student() {
	}

	public Student(int stdId, String stdName, String gender, String country, Date dob, String emailId,
			String password) {
		this.stdId = stdId;
		this.stdName = stdName;
		this.gender = gender;
		this.country = country;
		this.dob = dob;
		this.emailId = emailId;
		this.password = password;
	}
	
	

	public Course getCourse() {
		return Course;
	}
	public void setCourse(Course Course) {
		this.Course = Course;
	}

	public int getStdId() {
		return stdId;
	}
	public void setStdId(int stdId) {
		this.stdId = stdId;
	}

	public String getStdName() {
		return stdName;
	}
	public void setStdName(String stdName) {
		this.stdName = stdName;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}

	public Date getDob() {
		return dob;
	}
	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getPassword() {
		return this.password;
	}
//	public void setPassword(String password) {
//	    this.password = Base64.getEncoder().encodeToString(password.getBytes(StandardCharsets.UTF_8));
//	    this.password = BCrypt.hashpw(password, BCrypt.gensalt());
//	}
	
	public void setPassword(String password) {
		 String encryptedpassword = null; 
		  try   
	        {   
	            MessageDigest m = MessageDigest.getInstance("MD5");   
	            m.update(password.getBytes());  
	            byte[] bytes = m.digest();
	            StringBuilder s = new StringBuilder();  
	            for(int i=0; i< bytes.length ;i++)  
	            {  
	                s.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));  
	            } 
	            encryptedpassword = s.toString();  
	        }   
	        catch (NoSuchAlgorithmException e)   
	        {  
	            e.printStackTrace();  
	        }  
		this.password =encryptedpassword;
	}
	
//	public boolean checkPassword(String passwordToCheck) {
//	    return BCrypt.checkpw(passwordToCheck, this.password);
//	}

	@Override
	public String toString() {
		return "Student [stdId=" + stdId + ", stdName=" + stdName + ", gender=" + gender
				+ ", country=" + country + ", dob=" + dob + ", emailId=" + emailId + ", password=" + password
				+ ", Course=" + Course + "]";
	}

	
}


