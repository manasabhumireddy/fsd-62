// import { Component, OnInit } from '@angular/core';

// @Component({
//   selector: 'app-test1',
//   templateUrl: './test1.component.html',
//   styleUrl: './test1.component.css'
// })
// export class Test1Component implements OnInit {
//   person = {
//     id: 1,
//     name: 'John Doe',
//     age: 30,
//     hobbies: ['Reading', 'Traveling', 'Gaming'],
//     address: {
//       streetNo: '123 Main St',
//       city: 'New York',
//       state: 'NY'
//     }
//   }
//   constructor() { }

//   ngOnInit(): void {
//   }

// }

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test1',
  templateUrl: './test1.component.html',
  styleUrl: './test1.component.css'
})
export class Test1Component implements OnInit {
  person: any;

  constructor() {
    this.person = {
      id: 101,
      name: 'Harsha',
      age: 25,
      hobbies: ['Running', 'Music', 'Movies', 'GYM', 'Reading'],
      address: {streetNo: 111, city: 'Hyd', state: 'Telangana'}
    };
  }
  
  ngOnInit() {
  }
  submit() {
    alert("Id:" + this.person.id + "\n" + "Name: " + this.person.name);
    console.log(this.person);
  }

}

