import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrl: './test.component.css'
})
export class TestComponent implements OnInit{
  id :number;
  name :string;
  age :number;
  hobbies :any;
  address:any;
  person:any;

  constructor(){
    //alert("constructor invoked(called)..........")
    this.id=1;
    this.name='manasa';
    this.age=22;
    this.hobbies=['singing','dancing','running']
    this.address={streetNo:'101',city:'hyd',state:'Ap'}

  }
  ngOnInit() {
    //alert("ngOnInit invoked..........")
  }



}
