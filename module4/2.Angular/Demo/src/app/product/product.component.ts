import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  products: any;
  emailId: any;
  cartProducts: any[] = []; // Declare cartProducts as a property

  constructor(private service: EmpService) {
    this.emailId = localStorage.getItem('emailId'); 
  }

  ngOnInit(): void {
    this.service.getAllProducts().subscribe((data:any)=>{
      console.log(data);
      this.products = data;
    });
  }

  addToCart(product: any) {
    // Add product to the cart
    this.cartProducts.push(product);

    // Update cart count after adding the product
    this.service.updateCartCount(this.calculateCartCount());

    // Save cartProducts to local storage
    localStorage.setItem('cartProducts', JSON.stringify(this.cartProducts));
  }

  private calculateCartCount(): number {
    // Calculate the count of items in the cart
    return this.cartProducts.length;
  }
}