import { Component } from '@angular/core';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-showempbyid',
  templateUrl: './showempbyid.component.html',
  styleUrl: './showempbyid.component.css'
})
export class ShowempbyidComponent {

  emp: any;
  message: string;
  emailId: any;

  constructor(private service: EmpService) {
    this.message = "";
    this.emailId = localStorage.getItem('emailId');
  }

  async getEmployee(employee: any) {
    this.message = "";
    this.emp = null;
   
    await this.service.getEmployeeById(employee.empId).then((data: any) => {
      console.log(data);
      this.emp = data;
    });
   
    if (this.emp == null) {
      this.message = "Employee Record Not Found!!!";
    }   
  }
}