import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'empTitle'
})
export class EmpTitlePipe implements PipeTransform {
  transform(empName: any, gender: any): any {

    if (gender == 'Male')
      return 'MR.' + empName;
    else if (gender == 'Female')
      return 'Miss.' + empName;
    return empName;
  }

}
