import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrl: './header.component.css'
})
export class HeaderComponent implements OnInit {
  loginStatus: any;
  cartCount: number = 0; // Define cartCount property

  constructor(private service: EmpService) {}

  ngOnInit() {
    this.service.getUserLoginStatus().subscribe((data: any) => {
      this.loginStatus = data;
    });

    // Subscribe to cart count changes
    this.service.cartCount.subscribe(count => {
      this.cartCount = count;
    });
 
  }
  

}