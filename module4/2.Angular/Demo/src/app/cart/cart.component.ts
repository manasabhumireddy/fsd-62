import { JsonPipe } from '@angular/common';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrl: './cart.component.css'
})
export class CartComponent {

  cartProducts: any;
  products: any;
  emailId: any;
  total: number;
  cartCount: number;
  
  // constructor(private router: Router) {

  //   this.total = 0;

  //   this.emailId = localStorage.getItem('emailId');
  //   this.products  = localStorage.getItem('cartProducts');
  //   this.cartProducts = JSON.parse(this.products);

  //   console.log(this.cartProducts);
    
  //   this.cartProducts.forEach((element: any) => {
  //     this.total += element.price;
  //   });
  // }
  constructor(private router: Router, private empService: EmpService) {
    this.emailId = localStorage.getItem('emailId');
    this.total = 0;
    this.cartCount = 0;
    
    this.cartProducts = JSON.parse(localStorage.getItem('cartProducts') || '[]');

    this.empService.cartCount.subscribe((count: number) => {
      this.cartCount = count;
    });

    this.calculateTotal();
  }

  calculateTotal() {
    this.total = 0;
    this.cartProducts.forEach((element: any) => {
      this.total += element.price;
    });
  }

  purchase() {

    alert('Thank You for Purchasing');

    this.cartProducts = [];
    localStorage.removeItem('cartProducts');
    this.empService.updateCartCount(0);
    this.router.navigate(['products']);
  }
}